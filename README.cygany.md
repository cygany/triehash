This is the triehash package hunted from somewhere in the debian archives.

We do not have an upstream, but we should have.

This ia fork to become compilable under cygany, which is a debian overlay over cygwin.

Very likely, it still works also under the original debian systems.

Hopefully once we will have an upstream and we can set up this repo as the other cygany
sub-projects.

In our current state, it can be only compiled roughly so:

```
PERL5LIB=/usr/share/perl5 DEB_RULES_REQUIRES_ROOT=no debian/rules build binary
```

But soon it will be hopefully better.
